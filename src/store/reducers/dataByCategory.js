import {
    GET_DATA_BY_CATEGORY_BEGIN,
    GET_DATA_BY_CATEGORY_SUCCESS,
    GET_DATA_BY_CATEGORY_FAIL
} from '../actions/actionTypes';

const initialState = {
    loading: false,
    error: null,
    dataByCategories: []
}


const categoryMovieData = (state = initialState, action) => {
    const { type, payload, error } = action;
    
    switch (type) {
        default:
            return {
            ...state
            };
        case GET_DATA_BY_CATEGORY_BEGIN:
            return {
                ...state,
                loading: true,
                error: null,
            };
        case GET_DATA_BY_CATEGORY_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                dataByCategories: payload,
            };
        case GET_DATA_BY_CATEGORY_FAIL:
            return {
                ...state,
                loading: false,
                error: error,
                dataByCategories: []
            }
    }
}

export default categoryMovieData;