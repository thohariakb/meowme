import {
    GET_CHARACTERS_MOVIE_BEGIN,
    GET_CHARACTERS_MOVIE_SUCCESS,
    GET_CHARACTERS_MOVIE_FAIL
} from '../actions/actionTypes';

const initialState = {
    loading: false,
    error: null,
    charsMovies: []
}


const charsMovieData = (state = initialState, action) => {
    const { type, payload, error } = action;
    
    switch (type) {
        default:
            return {
            ...state
            };
        case GET_CHARACTERS_MOVIE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null,
            };
        case GET_CHARACTERS_MOVIE_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                charsMovies: payload,
            };
        case GET_CHARACTERS_MOVIE_FAIL:
            return {
                ...state,
                loading: false,
                error: error,
                charsMovies: []
            }
    }
}

export default charsMovieData;