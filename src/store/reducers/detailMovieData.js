import {
    GET_DETAIL_MOVIE_BEGIN,
    GET_DETAIL_MOVIE_SUCCESS,
    GET_DETAIL_MOVIE_FAIL
} from '../actions/actionTypes';

const initialState = {
    loading: false,
    error: null,
    detailMovies: []
}


const detailMovieData = (state = initialState, action) => {
    const { type, payload, error } = action;
    
    switch (type) {
        default:
            return {
            ...state
            };
        case GET_DETAIL_MOVIE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null,
            };
        case GET_DETAIL_MOVIE_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                detailMovies: payload,
            };
        case GET_DETAIL_MOVIE_FAIL:
            return {
                ...state,
                loading: false,
                error: error,
                detailMovies: []
            }
    }
}

export default detailMovieData;