import {SEARCH_MOVIE_ERROR, SEARCH_MOVIE_REQUEST, SEARCH_MOVIE_SUCCESS} from "../actions/actionTypes"

const initState = {
    loading: false,
    errorMessage: null,
    movies: [],

  };

const searchMovieData = (state = initState, action) => {
    const {type, payload, error} = action;
    switch(type) {
      case SEARCH_MOVIE_REQUEST:
        return {
          ...state,
          loading: true,
          error: null,
        };
      case SEARCH_MOVIE_SUCCESS:
        return {
          ...state,
          loading: false,
          movies: payload,
          error: null
        };
      case SEARCH_MOVIE_ERROR:
        return{
          ...state,
          loading: false,
          error: error,
          movies: [],
        }
      default:
        return {
          state
        }
    }
  }
  
export default searchMovieData