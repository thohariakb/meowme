import axios from "axios"
import {POST_SIGNIN_BEGIN, POST_SIGNIN_FAIL} from "./actionTypes"

//konidisi awal dari reducer false
const postSignInData = ( email, password ) => dispatch => {
    // console.log(token)
    dispatch({
        type: POST_SIGNIN_BEGIN,
        loading: true,
        error: null
    })
    axios
    .post("https://team-c.gabatch11.my.id/user/signin/",  
    {email, password})
    .then((res) => 
        // console.log('sign in', res)
        localStorage.setItem("token", res.data.token)
    )
    .catch((err) =>
        dispatch({
            type: POST_SIGNIN_FAIL,
            loading: false,
            error: err,
        })
    )
  }
  
export default postSignInData;