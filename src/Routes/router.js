import {BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from '../pages/Home';
import DetailOverview from '../pages/DetailOverview';
import DetailCharacters from '../pages/DetailCharacters';
import DetailReview from '../pages/DetailReview';
import SearchPage from '../pages/SearchPage';
import Profile from '../pages/Profile';


const Routes = () => {
    
    return(
        <Router>
            <Switch>
                <Route path="/" exact>
                    <Home />
                </Route>
                <Route path="/DetailOverview">
                    <DetailOverview/>
                </Route>
                <Route path="/DetailCharacters">
                    <DetailCharacters/>
                </Route>
                <Route path="/DetailReview">
                    <DetailReview/>
                </Route>
                {/* <Route path= "/SearchPage" component = {SearchPage}/> */}
                <Route path="/SearchPage" exact>
                    <SearchPage/>
                </Route>
                <Route path="/Profile" exact>
                    <Profile/>
                </Route>
            </Switch>
        </Router>
    );
}

export default Routes;