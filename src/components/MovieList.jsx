import React, {useState, useEffect, useReducer, useContext} from 'react';
import {CardColumns, Card, Button} from 'react-bootstrap';
import Pagination from './Pagination';
import CardMovie from './CardMovie';
import OverviewMovie from './OverviewMovie';

//redux
import { useSelector, useDispatch } from 'react-redux';
import {getDataMovie, getDataByCategory}  from '../store/actions/movieData';

const MovieList = () => {

  const dispatch = useDispatch();
  const movieData = useSelector((state) => state.movieData.movies);
  // console.log('movielist',movieData)
  // const dataByCategory = useSelector((state) => state.categoryMovieData.dataByCategories);

  useEffect(() => {
    dispatch(getDataMovie());
  }, [])


  // useEffect(() => {
  //   dispatch(getDataByCategory())
  // })

const [post, setPost] = useState(movieData);

const reducerCategoryAll = () => {
  const genre = setPost(movieData);
  return genre;
}

const reducerCategoryIsekai = () => {
  //  dispatch(getDataByCategory('isekai'));
  const genre = setPost(movieData.filter((movie) => movie.genre[1] === 'isekai'));
  return genre;
}

const reducerCategoryAction = () => {
  const genre = setPost(movieData.filter((movie) => movie.genre[1] === 'action'));
  return genre;
}

const reducerCategoryShonen = () => {
  const genre = setPost(movieData.filter((movie) => movie.genre[1]  === 'shonen'));
  return genre;
}

const reducerCategoryDrama = () => {
  const genre = setPost(movieData.filter((movie) => movie.genre[1]  === 'drama'));
  return genre;
}

const reducerCategoryComedy = () => {
  const genre = setPost(movieData.filter((movie) => movie.genre[1]  === 'comedy'));
  return genre; 
  }

const goToDetails = (item) => {
  localStorage.setItem('selectedItem', item);
  MovieList.history.push('/DetailOverview');
};

const [loading, setLoading] = useState(false);
const [currentPage, setCurrentPage] = useState(1)
const [postsPerPage, setPostsPerPage] = useState(6)

// GET current posts
const indexOfLastPost = currentPage * postsPerPage;
const indexOfFirstPost = indexOfLastPost - postsPerPage;
// console.log(post);
const currentPosts = post.slice(indexOfFirstPost, indexOfLastPost);
const currentMoviePosts = movieData.slice(indexOfFirstPost, indexOfLastPost);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  return(
    <>
    <div className="container">
      <div className="container">
        <div className="category">
          <h1>Browse by category</h1>
          <div className="homecategory-list d-flex flex-wrap">
            <a onClick={reducerCategoryAll}>All</a>
            <a onClick={reducerCategoryIsekai} alt="isekai">Isekai</a>
            <a onClick={reducerCategoryAction}>Action</a>
            <a onClick={reducerCategoryShonen}>Shonen</a>
            <a onClick={reducerCategoryDrama}>Drama</a>
            <a onClick={reducerCategoryComedy}>Comedy</a>
          </div>
        </div>
      </div>
      

      <div className="container">
        <div className="movie-list-pic">
          {/* <Row> */}
          <CardColumns className='w-100'>
          {
            (currentPosts.length > 0) ?
           currentPosts.map((item, i) => {
              return(
             
              <CardMovie
              key={i}
              item={item}
              onClick={() => goToDetails(item)}
              />

              );       
            })
             :
             currentMoviePosts.map((item, i) => {
              return(
             
              <CardMovie
              key={i}
              item={item}
              onClick={() => goToDetails(item)}
              />

              );       
            })
          }
          {/* </Row> */}
          </CardColumns>
        </div>
      </div>
      
      <div className="container pagination">
     
      <Pagination 
      postsPerPage={postsPerPage} 
      totalPosts={movieData.length}
      paginate={paginate}
      />
      
      </div>

    </div> 
    
    </>
  );
}

export default MovieList;