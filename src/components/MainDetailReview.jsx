import StarRatings from 'react-star-ratings';
import {useState, useEffect, useReducer, useContext} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {CardColumns, Container, Button, Form, Row, Col} from 'react-bootstrap';
import { getDataMovie, getReviewsMovie, postRatingMovie } from '../store/actions/movieData';
import peoplePic from './Assets/people.jpeg';
import '../pages/DetailPages.css';
import axios from 'axios';



const MainDetailReview = () => {

  const dispatch = useDispatch();
  const id = window.location.pathname.slice(-24);

  const movieData = useSelector((state) => state.movieData.movies);
  
  const reviewData = useSelector((state) => state.reviewsMovieData.reviewsMovies);
  console.log('review', reviewData);

  useEffect(() => {
    dispatch(getReviewsMovie(id));
  }, [])


  const [rating, setRating] = useState(0);
  console.log('rating', id);

  useEffect(() => {
    dispatch(postRatingMovie(rating, id))
},[rating])

  const changeRating = (rating, name, e) => {
      setRating({
          "value": rating
      });
  }



    return (
    <div className="container review">
        <div className="category">
            <div className="category-list d-flex flex-wrap">
            <a href={`/DetailOverview/${id}`}>Overview</a>
            <a href={`/DetailCharacters/${id}`}>Characters</a>
            <a className="active" href={`/DetailReview/${id}`}>Review</a>
            </div>
        </div>
        <div>
        <Container fluid className="review-box d-flex">
            <div><img src={peoplePic} className="review-pic"/></div>
        <Form>
            <Form.Group controlId="formBasicText">
               <h3><strong>Your Name</strong></h3>
            <div className="rating">
                   <StarRatings
                    numberOfStarts= {10}
                    starDimension="30px"
                    starRatedColor="yellow"
                    rating={rating}
                    changeRating={changeRating}
                    name='rating'
                    />
            </div>
            <Form.Control as="textarea" rows={3} cols={100} placeholder="Enter your review" style={{marginLeft: '0px'}}/>
            </Form.Group>   
            <Button><strong>Send</strong></Button>        
        </Form>
        </Container>
        <Container fluid>
        <Row md={12}>
        {
            reviewData?.map((item, i) => {
            return (
            <Container className="d-flex flex-nowrap review-box">
                <div>
                    <img src={peoplePic} className="review-pic"/>
                </div>
            <Row className="text-dark">
                <div>
                    <h3><strong>{item.user}</strong></h3>
                    <div className="rating">
                        <StarRatings
                        numberOfStarts= {10}
                        starDimension="30px"
                        starRatedColor="yellow"
                        rating={item.rating}
                        name='rating'
                        />
                    </div>
                    <p>
                    {item.review}
                    </p>
                </div>
            </Row>
            </Container>
            );              
            })
            
        }             
        </Row>
        </Container>
        </div>
    </div>
    )
}

export default MainDetailReview;