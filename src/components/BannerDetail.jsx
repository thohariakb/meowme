import React, {useEffect, useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getDataMovie, getDetailMovie } from '../store/actions/movieData';
import {Button, Modal} from 'react-bootstrap';
// import ReactStars from 'react-rating-stars-component';
import StarRatings from 'react-star-ratings';
import ReadMoreReact from 'read-more-react';
import '../../node_modules/react-modal-video/scss/modal-video.scss';
import ReactDOM from 'react-dom'
import ModalVideo from 'react-modal-video';

const BannerDetail = () => {

    // const [open, setOpen] = useState(false);

    // const toggle = () => {
    //     setOpen(!open);
    //   };

    const [isOpen, setOpen] = useState(false)

const id = window.location.pathname.slice(-24);

const dispatch = useDispatch();

const detailMovieData = useSelector((state) => state.detailMovieData.detailMovies);
console.log('cek bannerMovieData', detailMovieData?.urlTrailer?.split('/'));
const link = detailMovieData?.urlTrailer?.split('/');
console.log('link', link && link[3]); //ungguin linknya dlu lalau ambil index
// const rating = parseInt(movieData?.vote_average/2);
// console.log('rating', typeof rating);

useEffect(() => {
    dispatch(getDetailMovie(id));
}, [])

    return(
        <div className="w-100 h-50">
            <img className="img-fluid image" src={`https://team-c.gabatch11.my.id${detailMovieData.poster}`} alt="banner-img" />      
            <div className="detailbanner-content">
                    <h1>{detailMovieData.title}</h1>
                <div className="rating d-flex flex-wrap">
                    <StarRatings
                    numberOfStarts= {10}
                    starDimension="30px"
                    starRatedColor="yellow"
                    rating={detailMovieData.ratingAvg}
                    // onChangeRating= {rating}
                    
                    />
                    <p>{detailMovieData.vote_count} reviews</p>
  
                </div>
                <div>
                            {/* <div className="content-description">
                                <ReadMoreReact
                                text={detailMovieData.synopsis}
                                ideal={200}
                                min={150}
                                max={300}
                                readMoreText="Read more"
                                /> 
                            </div>  */}
                <p className="content-description">{detailMovieData.synopsis}</p>  
                </div>
                <div className="inline-button">
                    <button className="btn btn-primary" onClick={()=> setOpen(true)}>Watch Trailer</button>

                    {/* <button data-toggle="modal" data-target="#iframe" type="button" className="btn btn-primary">Watch Trailer</button> */}
                    <button style={{marginLeft: "2vh"}}type="button" className="btn btn-outline-primary">Add to Watchlist</button>
                    
                    <ModalVideo channel='youtube' autoplay isOpen={isOpen} videoId={link && link[3]} onClose={() => setOpen(false)} />

                    {/* <div className="modal fade"
                        id="iframe"
                        tabindex="-1" 
                        role="dialog" 
                        aria-labelledby="exampleModalLabel"
                        aria-hidden="true"
                        style={{marginTop: "25vh"}}
                        >
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                            <iframe width="560" height="315" 
                            href={detailMovieData.urlTrailer}
                            frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                            </div>
                        </div>
                    </div> */}

                </div>
            </div>
        </div>    
    );

}

export default BannerDetail;