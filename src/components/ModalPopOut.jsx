import React, { useEffect, useState } from "react"
import { ModalTitle } from "react-bootstrap"
import Modal from 'react-bootstrap/Modal'
import "./ModalPopOut.css"
import img1 from "./Assets/brandLogo.png"
import postRegisterData from "../store/actions/PostRegisterData"
import postSignInData from "../store/actions/PostSignInData"
import { useHistory } from "react-router-dom"
import { useDispatch } from "react-redux"


const ModalPopOut = ({...props}) => {
    const {show, showRegister, triggerSignIn, triggerRegister, setShowRegister, setShow} = props;

    const dispatch = useDispatch();
    const history = useHistory();
    const [name, setName] = useState("");
    const [password, setPassword] = useState("");
    const [email, setEmail] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [profileImage, setProfileImage] = useState("")
    const [fileImage, setFileImage] = useState("")
    const [previewImage, setPreviewImage] = useState("")

    const changeName = event => {
      setName(event.target.value);
    };

    const changePassword = event => {
      setPassword(event.target.value)
    };

    const changeConfirmPassword = event => {
      setConfirmPassword(event.target.value)
    };

    const changeEmail = event => {
      setEmail(event.target.value)
    };

    // untuk menentukan gambar yg akan ditampilkan dan dikirim ke database/
    const changeProfileImage = event => {
      let displayImage = event.target.files[0];
      if (displayImage){
        setProfileImage(displayImage);
        setFileImage(displayImage);
      }else{
        setProfileImage(null);
      }
    }

    // untuk memperlihatkan gambar di browser
    useEffect(() => {
      if (profileImage) {
        const reader = new FileReader();
        reader.onloadend = () => {
          setPreviewImage(reader.result);
        };
        reader.readAsDataURL(profileImage);
      } else {
        setPreviewImage(null);
      }
    }, [profileImage]);

    const handleRegister = event => {
      const imgData = new FormData();
      imgData.append("image", fileImage);
      dispatch(postRegisterData(name, email, password, confirmPassword, imgData));
      history.push("/Profile")
      event.preventDefault();
      setShowRegister(false)
    };

    const handleSignIn = event => {
      dispatch(postSignInData(email, password))
      history.push("/");
      event.preventDefault();
      setShow(false)
    };
    // console.log("name",name)
    // console.log("email",email)
    // console.log("pass",password)
    // console.log("confirmpass",confirmPassword)

    return(
    <>
      <Modal 
      show = {show} 
      onHide= {() => setShow(false)}
      size="lg"
      keyboard = "true"

      >
      <ModalTitle className="modal-title">
        <img 
        src={img1} 
        alt="logo"
        className="logo"/>
        Meowme
      </ModalTitle>
        <label 
        className="tag" 
        >Email</label>
        <input 
        value= {email} 
        onChange={changeEmail}
        type="email" 
        placeholder="email" className="modal-input"/>
        <label 
        className="tag" 
        >Password</label>
        <input 
        onChange={changePassword}
        type="password" 
        placeholder="password" className="modal-input"/>
        <button 
        className="btn-sign-in" 
        onClick={handleSignIn}>Sign In</button>
        <div className="modal-sign-in">
          <h5>Don't have an account?</h5>
          <button 
          className="btn-register" 
          onClick={triggerRegister}>Register</button>
        </div>        
      </Modal>

      <Modal  
        show = {showRegister}
        onHide = {()=> setShowRegister(false)}
        size="lg" 
        keyboard = "true"
      >
      <ModalTitle className="modal-title">
        <img 
        src= {img1} 
        alt="logo"
        className="logo"/>
        Meowme
      </ModalTitle>
        <label 
        className="tag" 
        >Full Name</label>
        <input 
        value={name} 
        onChange={changeName}
        type="text" 
        placeholder="full name" 
        className="modal-input"/>
        <label 
        className="tag" 
        >Email</label>
        <input 
        value={email} 
        onChange={changeEmail}
        type="email" 
        placeholder="email" 
        className="modal-input"/>
        <label 
        className="tag" 
        >Password</label>
        <input 
        value={password} 
        onChange={changePassword}
        type="password" 
        placeholder="password" 
        className="modal-input"/>
        <label 
        className="tag" 
        >Confirm Password</label>
        <input 
        value = {confirmPassword}
        onChange={changeConfirmPassword}
        type="password" 
        placeholder="password" className="modal-input"/>
        
        <label 
        className="tag" 
        >Profile Image</label>
        <form>
          <img src={previewImage} alt="profile" className="modal-image"/>
          <input 
          onChange={changeProfileImage}
          type="file"  
          // className="modal-input"
          accept = "image/*"
          />
        </form>
        <button 
        className="btn-sign-in" 
        onClick={handleRegister}>Register</button>
        <div className="modal-sign-in">
          <h5>Already have an account?</h5>
          <button 
          className="btn-register" 
          onClick ={triggerSignIn}>Sign In</button>
        </div>
      </Modal>
  </>
  )
}
export default ModalPopOut