import React, {useEffect, useState} from 'react';
import Navigation from "../components/Navbar"
import Footer from '../components/Footer';
import BannerDetail from '../components/BannerDetail';
import MainDetailCharacters from '../components/MainDetailCharacters';
import './DetailPages.css'
import { useSelector, useDispatch } from 'react-redux';
import {getDetailMovie} from '../store/actions/movieData';

const DetailCharacters = () => {

    const dispatch = useDispatch();
    const id = window.location.pathname.slice(-24);

    // const detailMovieData = useSelector((state) => state.detailMovieData.detailMovies);
  
    useEffect(() => {
        dispatch(getDetailMovie(id));
    }, [])

    return(
        <div>
            <Navigation/>
            <BannerDetail/>
            <MainDetailCharacters/>
            <Footer/>
        </div>
    );
}

export default DetailCharacters;